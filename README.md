## 用Java编写的CMS系统客户端简单说明


### 项目简介

- J2EE课程期末项目的客户端
- 用Java编写的简单的CMS（内容管理）系统

### 功能特征

- 客户端具备网站基本特征
- 数据库存储数据，动态管理网站内容
- 具有`网站logo`、`导航区`、`图片轮播区`、`单页区`、`新闻列表区`、`多媒体展示区`、`底部版权说明`等模块。

### 环境依赖

- Angular CLI: 8.0.3
- Node: 12.16.1
- OS: windows x64
- Bootstrap -v 4.4.1
- PrimeNG 8.1.1

### 部署步骤

- 本地部署

下载项目到本地，用`WebStorm`导入项目。终端输入命令：`ng serve`启动。浏览器：`locahost:4200`访问。
注意：项目为`前后端分离式`开发。需要配合后端获取`数据`。

- 云部署

由于项目`还未完成`，所以暂时不提供客户端云部署文件。可以自行下载项目，然后用ng build --prod命令打包后上传云服务器。

### 演示地址

- [我的民族文化之旅](https://www.mzwhzy.com)  
- 或者浏览器输入：www.mzwhzy.com

### 我的个人技术博客

- [微笑涛声](https://www.cztcms.cn)  
- 或者浏览器输入：www.cztcms.cn

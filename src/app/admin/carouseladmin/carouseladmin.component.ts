import { Component, OnInit } from '@angular/core';
import {Carousel} from "../../model/Carousel";
import {CarouselService} from "../../service/carousel.service";
import {DialogService} from "primeng/api";
import {CarouseleditComponent} from "../carouseledit/carouseledit.component";


@Component({
  selector: 'app-carouseladmin',
  templateUrl: './carouseladmin.component.html',
  styleUrls: ['./carouseladmin.component.css']
})
export class CarouseladminComponent implements OnInit {
  cl:Array<Carousel>;
  constructor(private carouselservice:CarouselService,
              private dialogService: DialogService) {
    this.cl=new Array();
    this.loadCarouselList();
  }

  ngOnInit() {
    //this.loadCarouselList(); //实例化
  }

  loadCarouselList(){
    this.carouselservice.getCarouseListMap()
      .then((data:any)=>{
        this.cl=new Array();
        this.cl=data.list;

      })
  }

  add(){
    const ref = this.dialogService.open(CarouseleditComponent, {

      header: '添加轮播图'

    });

    //订阅窗口关闭事件
    ref.onClose.subscribe((result: Carousel) => {

      //从窗口回传的值

      if(result){
        this.carouselservice.saveCarousel(result)
          .then((data:any)=>{
            if(data.msg=='ok'){
              this.loadCarouselList();
            }
            else {
              //提示错误;
            }



          })
      }


    });



  }


  delete(id:string){
    this.carouselservice.deleteCarousel(id)
      .then((data:any)=>{
        if(data.msg=='ok'){
          this.loadCarouselList();
        }
        else{
          //弹出错误信息
        }


      })


  }



  update(id:string){
    //从服务器端获取最近的需要修改的值
    this.carouselservice.getSingleCarousel(id)
      .then((data:any)=>{
        if(data.msg=='ok'){
          let carousel=new Carousel();
          carousel=data.obj;
          //将需修改的值，传到修改窗口
          const ref = this.dialogService.open(CarouseleditComponent, {
            data: {
              carousel: carousel
            },
            header: '修改轮播',

          });
//订阅窗口关闭事件
          ref.onClose.subscribe((result: Carousel) => {

            //从窗口回传的值

            if(result){
              this.carouselservice.saveCarousel(result)
                .then((data:any)=>{
                  if(data.msg=='ok'){
                    this.loadCarouselList();
                  }
                  else {
                    //提示错误;
                  }



                })
            }


          });


        }


      })




  }


}

import { Component, OnInit } from '@angular/core';
import {Carousel} from "../../model/Carousel";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-carouseledit',
  templateUrl: './carouseledit.component.html',
  styleUrls: ['./carouseledit.component.css']
})
export class CarouseleditComponent implements OnInit {
  HOST:string;
  carousel:Carousel;
  constructor(private carouselservice:CarouselService,
              public ref: DynamicDialogRef,
             public config: DynamicDialogConfig,
              private sysconfig:ConfigService
              ) {
    this.carousel=new Carousel();
  }
  ngOnInit() {

    if(this.config.data){
      //修改操作
      this.carousel=new Carousel();
      this.carousel=this.config.data.carousel;
    }
    else{
      //添加操作
      this.carousel=new Carousel();
    }

  }

  save(){
    this.closeWindow();
  }

  onFileChanged(event:any){

    let file=  event.target.files[0];
    //console.log("file"+file);

    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.carousel.picurl=this.sysconfig.HOST+"/public/"+data.filename;
          //this.carousel.picurl=data.filename;

        }
      })

  }

    closeWindow(){
      //准备传到父窗口的对象 this.carousel
      this.ref.close(this.carousel);

  }

}

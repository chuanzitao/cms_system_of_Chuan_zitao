import { Component, OnInit } from '@angular/core';
import {Footer} from "../../model/Footer";
import {FooterService} from "../../service/footer.service";
import {DialogService} from "primeng/api";
import {FootereditComponent} from "../footeredit/footeredit.component";

@Component({
  selector: 'app-footeradmin',
  templateUrl: './footeradmin.component.html',
  styleUrls: ['./footeradmin.component.css']
})
export class FooteradminComponent implements OnInit {

  fl: Array<Footer>;   //定义数组变量nl
  constructor(private footerService: FooterService, public dialogService: DialogService) {
    this.fl = new Array();  //实例化对象
  }

  ngOnInit() {
    this.loadFooterList(); //方法的具体调用
  }

  //获取新闻表数据，存储在nl中
  loadFooterList() {
    this.footerService.getFooterList()
      .then((data: any) => {
        this.fl = data;
      })
  }

  //新闻添加操作
  add() {
    const ref = this.dialogService.open(FootereditComponent, {
      header: '添加版权说明',
    });
    //当弹出窗口关闭时触发的事件
    ref.onClose.subscribe((footer:Footer) => {
      if(footer){
        console.log("弹出层关闭时回传的值"+footer.title1);
        this.footerService.saveFooter(footer)
          .then((data:any)=>{
            if(data.msg=='ok'){
              this.loadFooterList();
            }
            else {
              //弹出错误信息
            }
          })
      }
    });
  }

  // 新闻修改操作
  update(id:string){
    let footer:Footer;
    footer=new Footer();
    this.footerService.getSingleFooterById(id)
      .then((data:any)=>{
        if(data){
          footer=data;
          const ref = this.dialogService.open(FootereditComponent, {
            data: {
              "footer":footer
            },
            header: '修改版权说明',
          });
          //当弹出窗口关闭时触发的事件
          ref.onClose.subscribe((footer:Footer) => {
            if(footer){
              console.log("弹出层关闭时回传的值"+footer.title1);
              this.footerService.saveFooter(footer)
                .then((data:any)=>{
                  if(data.msg=='ok'){
                    this.loadFooterList();
                  }
                  else {
                    //弹出错误信息
                  }
                })
            }
          });
        }
      })
  }

  //新闻删除操作
  delete(id:string){
    this.footerService.deleteFooter(id)
      .then((data:any)=>{
        if(data.msg=='ok'){
          this.loadFooterList();
        }
        else{
          //弹出报错信息
        }
      })
  }
}

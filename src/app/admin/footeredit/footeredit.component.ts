import { Component, OnInit } from '@angular/core';
import {Footer} from "../../model/Footer";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-footeredit',
  templateUrl: './footeredit.component.html',
  styleUrls: ['./footeredit.component.css']
})
export class FootereditComponent implements OnInit {

  footer:Footer; //定义变量
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.footer=new Footer();//实例化对象
  }

  //添加操作和修改操作调用同一个方法，修改需要主键。不为空，则执行修改操作
  ngOnInit() {
    //有数据，修改操作
    if(this.config.data){
      this.footer=new Footer();
      this.footer=this.config.data.footer;
    }
    //否则添加操作
    else{
      this.footer=new Footer();
    }
  }

  save(){
    // console.dir(this.news); //控制台测试
    this.closeDialog();
  }

  closeDialog(){
    this.ref.close(this.footer); //点击保存以后关闭弹窗的编辑窗口
  }
}


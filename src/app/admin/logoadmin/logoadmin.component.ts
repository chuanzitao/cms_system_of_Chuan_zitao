import { Component, OnInit } from '@angular/core';
import {Logo} from "../../model/Logo";
import {LogoService} from "../../service/logo.service";
import {DialogService} from "primeng/api";
import {LogoeditComponent} from "../logoedit/logoedit.component";

@Component({
  selector: 'app-logoadmin',
  templateUrl: './logoadmin.component.html',
  styleUrls: ['./logoadmin.component.css']
})
export class LogoadminComponent implements OnInit {

  ll:Array<Logo>
  constructor(private logoservice:LogoService
    ,private dialogService: DialogService) {
    this.ll=new Array();

  }

  ngOnInit() {
    this.loadLogoList();

  }


  //从服务器端获取单页列表数据
  loadLogoList(){
    this.logoservice.getLogoList()
      .then((data:any)=>{
        this.ll=new Array();
        this.ll=data;
      })
  }

  add(){
    //弹出编辑窗口
    const ref = this.dialogService.open(LogoeditComponent, {
      header: '添加Logo'

    });

    //订阅窗口关闭事件

    ref.onClose.subscribe((logo: Logo) => {
      if (logo) {
        //保存数据到服务器
        this.logoservice.saveLogo(logo)
          .then((data:any)=>{
            if(data.msg=='ok'){
              this.loadLogoList();
            }
            else{
              //提示错误
            }


          })




      }
    });



  }

  //删除

  delete(id:string){

    this.logoservice.deleteLogo(id)
      .then((data:any)=>{
        if(data.msg=='ok'){
          this.loadLogoList();
        }
        else{
          //弹出错误信息;
        }


      })


  }

  update(id:string){
    //从服务器端读取数据
    this.logoservice.getSingleLogoById(id)
      .then((data:any)=>{
        if(data){
          let logo:Logo;
          logo=new Logo();
          logo=data;

          //发送要编辑的数据到编辑窗口
          //弹出编辑窗口
          const ref = this.dialogService.open(LogoeditComponent, {
            header: '修改lOGO',
            data:{
              "logo":logo
            }

          });

          //订阅窗口关闭事件

          ref.onClose.subscribe((logo: Logo) => {
            if (logo) {
              //保存数据到服务器
              this.logoservice.saveLogo(logo)
                .then((data:any)=>{
                  if(data.msg=='ok'){
                    this.loadLogoList();
                  }
                  else{
                    //提示错误
                  }


                })




            }
          });


        }


      });




  }



}


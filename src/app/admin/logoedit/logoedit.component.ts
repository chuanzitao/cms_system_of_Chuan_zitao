import { Component, OnInit } from '@angular/core';
import {Logo} from "../../model/Logo";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-logoedit',
  templateUrl: './logoedit.component.html',
  styleUrls: ['./logoedit.component.css']
})
export class LogoeditComponent implements OnInit {

  logo:Logo;

  constructor(private carouselservice:CarouselService,
              private sysconfig:ConfigService
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.logo=new Logo();
  }

  ngOnInit() {
    if(this.config.data){
      //执行修改操作
      this.logo=new Logo();
      this.logo=this.config.data.logo;

    }
    else {
      //执行添加操作
      this.logo=new Logo();
    }


  }

  save(){

    //console.dir(this.singlepage);
    this.closeWindow();

  }


  //图片1上传方法
  onFileChanged(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.logo.logourl=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }


  //图片2上传方法
  onFileChanged1(event:any){
    let file:string=  event.target.files[0];
    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.logo.sloganurl=this.sysconfig.HOST+"/public/"+data.filename;
        }
      })

  }



  closeWindow(){
    this.ref.close(this.logo);

  }

}

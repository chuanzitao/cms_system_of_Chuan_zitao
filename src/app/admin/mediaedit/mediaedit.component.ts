import { Component, OnInit } from '@angular/core';
import {Media} from "../../model/Media";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-mediaedit',
  templateUrl: './mediaedit.component.html',
  styleUrls: ['./mediaedit.component.css']
})
export class MediaeditComponent implements OnInit {

  media :Media;

  constructor(private carouselservice:CarouselService,
              private sysconfig:ConfigService
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.media=new Media();
  }

  ngOnInit() {
    if(this.config.data){
      //执行修改操作
      this.media=new Media();
      this.media=this.config.data.media;

    }
    else {
      //执行添加操作
      this.media=new Media();
    }


  }

  save(){

    //console.dir(this.singlepage);
    this.closeWindow();

  }


  //图片上传方法
  onFileChanged(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.media.tup=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }

  //视频上传方法
  onFileChangedsp(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.media.url=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }


  closeWindow(){
    this.ref.close(this.media);

  }

}

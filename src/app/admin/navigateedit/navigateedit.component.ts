import { Component, OnInit } from '@angular/core';
import {Navigate} from "../../model/Navigate";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-navigateedit',
  templateUrl: './navigateedit.component.html',
  styleUrls: ['./navigateedit.component.css']
})
export class NavigateeditComponent implements OnInit {

  navigate:Navigate; //定义变量
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.navigate=new Navigate();//实例化对象
  }

  //添加操作和修改操作调用同一个方法，修改需要主键。不为空，则执行修改操作
  ngOnInit() {
    //有数据，修改操作
    if(this.config.data){
      this.navigate=new Navigate();
      this.navigate=this.config.data.navigate;
    }
    //否则添加操作
    else{
      this.navigate=new Navigate();
    }
  }

  save(){
    // console.dir(this.navigate); //控制台测试
    this.closeDialog();
  }

  closeDialog(){
    this.ref.close(this.navigate); //点击保存以后关闭弹窗的编辑窗口
  }
}

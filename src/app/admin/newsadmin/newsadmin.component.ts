import { Component, OnInit } from '@angular/core';
import {NewsService} from "../../service/news.service";
import {News} from "../../model/News";
import {DialogService} from "primeng/api";
import {NewseditComponent} from "../newsedit/newsedit.component";
import {ToastService} from "../../service/toast.service";

@Component({
  selector: 'app-newsadmin',
  templateUrl: './newsadmin.component.html',
  styleUrls: ['./newsadmin.component.css']
})
export class NewsadminComponent implements OnInit {
  nl: Array<News>;   //定义数组变量nl
  page:number;
  pagesize:number;
  total:number;

  constructor(private newsService: NewsService, public dialogService: DialogService
  , private toastservice:ToastService) {
    this.nl = new Array();  //实例化对象
  }

//在系统加载时运行一次，注意：只运行一次
  ngOnInit() {
    //this.loadNewsList();
    this.page=0;
    this.pagesize=4;
    this.getNewsByPage();
  }
  //获取新闻表数据，存储在nl中
  loadNewsList() {
    this.newsService.getNewsList()
      .then((data: any) => {
        this.nl = data;
      })
  }

  //新闻添加操作
  addNews() {
    const ref = this.dialogService.open(NewseditComponent, {
      header: '添加新闻',
    });
    //当弹出窗口关闭时触发的事件
    ref.onClose.subscribe((news:News) => {
      if(news){
        console.log("弹出层关闭时回传的值"+news.title1);
        this.newsService.saveNews(news)
          .then((data:any)=>{
            if(data.msg=='ok'){
              this.loadNewsList();
            }
            else {
              //弹出错误信息
            }
          })
      }
    });
  }

  // 新闻修改操作
  updateNews(id:string){
    let news:News;
    news=new News();
    this.newsService.getSingleNewsById(id)
      .then((data:any)=>{
        if(data){
          news=data;
          const ref = this.dialogService.open(NewseditComponent, {
            data: {
              "news":news
            },
            header: '修改新闻',
          });
          //当弹出窗口关闭时触发的事件
          ref.onClose.subscribe((news:News) => {
            if(news){
              console.log("弹出层关闭时回传的值"+news.title1);
              this.newsService.saveNews(news)
                .then((data:any)=>{
                  if(data.msg=='ok'){
                    this.loadNewsList();
                  }
                  else {
                    //弹出错误信息
                  }
                })
            }
          });
        }
      })
  }

  //新闻删除操作
  deleteNews(id:string){
    this.newsService.deleteNews(id)
      .then((data:any)=>{
        if(data.msg=='ok'){
          this.loadNewsList();
        }
        else{
          //弹出报错信息
        }
      })
  }

  //通过分页获取news数据
  getNewsByPage(){
    this.newsService.getNewsListBypage(this.page,this.pagesize)
      .then((data:any)=>{
        if(data.msg=='ok'){

          this.nl=new Array();
          this.nl=data.obj.content;
          this.total=data.obj.totalElements;


        }
        else if(data.msg=='error'){

          this.toastservice.showError("服务器异常，请重试");

        }
        else {
          this.toastservice.showError("通讯异常，请重试");
        }
      })



  }

//捕获分页组件
  onPageChange(e:any){

    console.dir(e);
    this.page=e.page;
    this.getNewsByPage();

  }


}

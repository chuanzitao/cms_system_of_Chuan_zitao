import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";
import {News} from "../../model/News";

@Component({
  selector: 'app-newsedit',
  templateUrl: './newsedit.component.html',
  styleUrls: ['./newsedit.component.css']
})
export class NewseditComponent implements OnInit {
  news:News; //定义变量
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.news=new News();//实例化对象
  }

  //添加操作和修改操作调用同一个方法，修改需要主键。不为空，则执行修改操作
  ngOnInit() {
    //有数据，修改操作
    if(this.config.data){
      this.news=new News();
      this.news=this.config.data.news;
    }
    //否则添加操作
    else{
      this.news=new News();
    }
  }

  save(){
    // console.dir(this.news); //控制台测试
    this.closeDialog();
  }

  closeDialog(){
    this.ref.close(this.news); //点击保存以后关闭弹窗的编辑窗口
  }
}

import { Component, OnInit } from '@angular/core';
import {Picture} from "../../model/Picture";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-pictureedit',
  templateUrl: './pictureedit.component.html',
  styleUrls: ['./pictureedit.component.css']
})
export class PictureeditComponent implements OnInit {

  picture:Picture;

  constructor(private carouselservice:CarouselService,
              private sysconfig:ConfigService
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.picture=new Picture();
  }

  ngOnInit() {
    if(this.config.data){
      //执行修改操作
      this.picture=new Picture();
      this.picture=this.config.data.picture;

    }
    else {
      //执行添加操作
      this.picture=new Picture();
    }


  }

  save(){

    //console.dir(this.singlepage);
    this.closeWindow();

  }


  //图片上传方法
  onFileChanged(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.picture.picurl=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }


  closeWindow(){
    this.ref.close(this.picture);

  }

}


import { Component, OnInit } from '@angular/core';
import {Product} from "../../model/Product";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-productedit',
  templateUrl: './productedit.component.html',
  styleUrls: ['./productedit.component.css']
})
export class ProducteditComponent implements OnInit {
  product :Product;

  constructor(private carouselservice:CarouselService,
              private sysconfig:ConfigService
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.product=new Product();
  }

  ngOnInit() {
    if(this.config.data){
      //执行修改操作
      this.product=new Product();
      this.product=this.config.data.product;

    }
    else {
      //执行添加操作
      this.product=new Product();
    }


  }

  save(){

    //console.dir(this.singlepage);
    this.closeWindow();

  }


  //图片上传方法
  onFileChanged(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.product.url=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }


  closeWindow(){
    this.ref.close(this.product);

  }

}

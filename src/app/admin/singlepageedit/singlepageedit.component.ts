import { Component, OnInit } from '@angular/core';
import {SinglePage} from "../../model/SinglePage";
import {CarouselService} from "../../service/carousel.service";
import {ConfigService} from "../../service/config.service";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";

@Component({
  selector: 'app-singlepageedit',
  templateUrl: './singlepageedit.component.html',
  styleUrls: ['./singlepageedit.component.css']
})
export class SinglepageeditComponent implements OnInit {

  singlepage:SinglePage;

  constructor(private carouselservice:CarouselService,
              private sysconfig:ConfigService
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.singlepage=new SinglePage();
  }

  ngOnInit() {
    if(this.config.data){
      //执行修改操作
      this.singlepage=new SinglePage();
      this.singlepage=this.config.data.singlepage;

    }
    else {
      //执行添加操作
      this.singlepage=new SinglePage();
    }


  }

  save(){

    //console.dir(this.singlepage);
    this.closeWindow();

  }


  //图片上传方法
  onFileChanged(event:any){

    let file:string=  event.target.files[0];
    //console.log("file"+file);


    this.carouselservice.uploadPic(file)
      .then((data:any)=>{
        if(data.msg=='ok'){

          console.log("serverfile "+data.filename);
          this.singlepage.cover=this.sysconfig.HOST+"/public/"+data.filename;

        }
      })

  }


  closeWindow(){
    this.ref.close(this.singlepage);

  }

}

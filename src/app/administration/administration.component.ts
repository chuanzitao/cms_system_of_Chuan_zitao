import { Component, OnInit } from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {DialogService} from "primeng/api";

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
  }

  showLogin(){

    const ref = this.dialogService.open(LoginComponent, {
      header: '用户登录'

    });

  }

}

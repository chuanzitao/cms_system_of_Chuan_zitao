import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IndexComponent} from "./index/index.component";
import {SqdetailComponent} from "./sqdetail/sqdetail.component";
import {NewstailComponent} from "./newstail/newstail.component";
import {AudiolistforindexComponent} from "./audiolistforindex/audiolistforindex.component";
import {AudioplayComponent} from "./audioplay/audioplay.component";
import {VideolistforindexComponent} from "./videolistforindex/videolistforindex.component";
import {VideoplayComponent} from "./videoplay/videoplay.component";
import {LogoComponent} from "./logo/logo.component";
import {FooterComponent} from "./footer/footer.component";
import {NavigatedatilComponent} from "./navigatedatil/navigatedatil.component";
import {NewsadminComponent} from "./admin/newsadmin/newsadmin.component";
import {CarouseladminComponent} from "./admin/carouseladmin/carouseladmin.component";
import {SinglepageadminComponent} from "./admin/singlepageadmin/singlepageadmin.component";
import {NavigateadminComponent} from "./admin/navigateadmin/navigateadmin.component";
import {ProductadminComponent} from "./admin/productadmin/productadmin.component";
import {MediaadminComponent} from "./admin/mediaadmin/mediaadmin.component";
import {LogoadminComponent} from "./admin/logoadmin/logoadmin.component";
import {FooteradminComponent} from "./admin/footeradmin/footeradmin.component";
import {PictureforindexComponent} from "./pictureforindex/pictureforindex.component";
import {PictureadminComponent} from "./admin/pictureadmin/pictureadmin.component";
import {P401Component} from "./p401/p401.component";
import {P403Component} from "./p403/p403.component";
import {AdministrationComponent} from "./administration/administration.component";
import {NavigationadminComponent} from "./navigationadmin/navigationadmin.component";



const routes: Routes = [
  {path:'index',component:IndexComponent},
  {path:'sqdetail',component:SqdetailComponent},
  {path:'newstail',component:NewstailComponent},
  {path:'audiolistforindex',component:AudiolistforindexComponent},
  {path:'audioplay',component:AudioplayComponent},
  {path:'videolistforindex',component:VideolistforindexComponent},
  {path:'videoplay',component:VideoplayComponent},
  {path:'logo',component:LogoComponent},
  {path:'footer',component:FooterComponent},
  {path:'adminnews',component:NewsadminComponent},
  {path:'navigatedatil',component:NavigatedatilComponent},
  {path:'carouseladmin',component:CarouseladminComponent},
  {path:'singlepageadmin',component:SinglepageadminComponent},
  {path:'navigateadmin',component:NavigateadminComponent},
  {path:'productadmin',component:ProductadminComponent},
  {path:'mediaadmin',component:MediaadminComponent},
  {path:'logoadmin',component:LogoadminComponent},
  {path:'footeradmin',component:FooteradminComponent},
  {path:'pictureforindex',component:PictureforindexComponent},
  {path:'pictureadmin',component:PictureadminComponent},
  {path:'p401',component:P401Component},
  {path:'p403',component:P403Component},
  {path:'admin',component:AdministrationComponent},
  {path:'navigationadmin',component:NavigationadminComponent},
  {path:'**',component:IndexComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { NavigateComponent } from './navigate/navigate.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CarouselforindexComponent } from './carouselforindex/carouselforindex.component';
import { SplistforindexComponent } from './splistforindex/splistforindex.component';
import { NewslistforindexComponent } from './newslistforindex/newslistforindex.component';
import { SqdetailComponent } from './sqdetail/sqdetail.component';
import { NewstailComponent } from './newstail/newstail.component';
import { AudiolistforindexComponent } from './audiolistforindex/audiolistforindex.component';
import { AudioplayComponent } from './audioplay/audioplay.component';
import {VgCoreModule} from "videogular2/compiled/src/core/core";
import {VgControlsModule} from "videogular2/compiled/src/controls/controls";
import {VgOverlayPlayModule} from "videogular2/compiled/src/overlay-play/overlay-play";
import {VgBufferingModule} from "videogular2/compiled/src/buffering/buffering";
import { VideolistforindexComponent } from './videolistforindex/videolistforindex.component';
import { VideoplayComponent } from './videoplay/videoplay.component';
import { LogoComponent } from './logo/logo.component';
import { FooterComponent } from './footer/footer.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { TohtmlPipe } from './pipe/tohtml.pipe';
import { NavigatedatilComponent } from './navigatedatil/navigatedatil.component';
import { NewsadminComponent } from './admin/newsadmin/newsadmin.component';
import {ButtonModule} from "primeng/button";
import {DynamicDialogModule} from "primeng/dynamicdialog";
import { NewseditComponent } from './admin/newsedit/newsedit.component';
import {DialogService, MessageService} from "primeng/api";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CardModule} from "primeng/card";
import {EditorModule} from "primeng/editor";
import {FormsModule} from "@angular/forms";
import { CarouseladminComponent } from './admin/carouseladmin/carouseladmin.component';
import { CarouseleditComponent } from './admin/carouseledit/carouseledit.component';
import { SinglepageadminComponent } from './admin/singlepageadmin/singlepageadmin.component';
import { SinglepageeditComponent } from './admin/singlepageedit/singlepageedit.component';
import { NavigateadminComponent } from './admin/navigateadmin/navigateadmin.component';
import { NavigateeditComponent } from './admin/navigateedit/navigateedit.component';
import { ProductadminComponent } from './admin/productadmin/productadmin.component';
import { ProducteditComponent } from './admin/productedit/productedit.component';
import { MediaadminComponent } from './admin/mediaadmin/mediaadmin.component';
import { MediaeditComponent } from './admin/mediaedit/mediaedit.component';
import { LogoadminComponent } from './admin/logoadmin/logoadmin.component';
import { LogoeditComponent } from './admin/logoedit/logoedit.component';
import { FooteradminComponent } from './admin/footeradmin/footeradmin.component';
import { FootereditComponent } from './admin/footeredit/footeredit.component';
import { PictureforindexComponent } from './pictureforindex/pictureforindex.component';
import { PictureadminComponent } from './admin/pictureadmin/pictureadmin.component';
import { PictureeditComponent } from './admin/pictureedit/pictureedit.component';
import { P401Component } from './p401/p401.component';
import { P403Component } from './p403/p403.component';
import { LoginComponent } from './login/login.component';
import { IonicStorageModule } from '@ionic/storage';
import {MyHttpInterceptor} from "./service/MyHttpInterceptor";
import {AuthService} from "./service/auth.service";
import { AdministrationComponent } from './administration/administration.component';
import { NavigationadminComponent } from './navigationadmin/navigationadmin.component';
import {ToastModule} from "primeng/toast";
import {PaginatorModule} from 'primeng/paginator';






@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    NavigateComponent,
    CarouselforindexComponent,
    SplistforindexComponent,
    NewslistforindexComponent,
    SqdetailComponent,
    NewstailComponent,
    AudiolistforindexComponent,
    AudioplayComponent,
    VideolistforindexComponent,
    VideoplayComponent,
    LogoComponent,
    FooterComponent,
    TohtmlPipe,
    NavigatedatilComponent,
    NewsadminComponent,
    NewseditComponent,
    CarouseladminComponent,
    CarouseleditComponent,
    SinglepageadminComponent,
    SinglepageeditComponent,
    NavigateadminComponent,
    NavigateeditComponent,
    ProductadminComponent,
    ProducteditComponent,
    MediaadminComponent,
    MediaeditComponent,
    LogoadminComponent,
    LogoeditComponent,
    FooteradminComponent,
    FootereditComponent,
    PictureforindexComponent,
    PictureadminComponent,
    PictureeditComponent,
    P401Component,
    P403Component,
    LoginComponent,
    AdministrationComponent,
    NavigationadminComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    HttpClientModule,
    ButtonModule,
    DynamicDialogModule,
    CardModule,
    EditorModule,
    FormsModule,
    IonicStorageModule.forRoot({
      name: 'ynmdedu',
      driverOrder: ['localstorage']

    }),
    ToastModule,
    PaginatorModule


  ],
  entryComponents: [
    NewseditComponent,
    CarouseleditComponent,
    SinglepageeditComponent,
    NavigateeditComponent,
    ProducteditComponent,
    MediaeditComponent,
    LogoeditComponent,
    FootereditComponent,
    PictureeditComponent,
    LoginComponent
],
  providers: [
    DialogService,{
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true //每次HTTP请求时执行
    },
    AuthService,
    MessageService
    ],

  bootstrap: [AppComponent]
})
export class AppModule { }

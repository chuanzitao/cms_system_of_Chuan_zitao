import { Component, OnInit } from '@angular/core';
import {Product} from "../model/Product";
import {Router} from "@angular/router";
import {ProductService} from "../service/product.service";

@Component({
  selector: 'app-audiolistforindex',
  templateUrl: './audiolistforindex.component.html',
  styleUrls: ['./audiolistforindex.component.css']
})
export class AudiolistforindexComponent implements OnInit {
  al:Array<Product>;   //定义al变量
  constructor(private productservice:ProductService
    ,private router:Router) {
    this.al=new Array();  //实例化对象
  }

  ngOnInit() {
    this.getProductList();
  }

  //多个查询的实现
  getProductList(){
    this.productservice.getProductList()
      .then((data:any)=>{
        this.al=new Array();
        this.al=data; //al取得表的所有数据

      })
  }

  //单个查询的方法
  goToProductDetail(id:string){
                //定义跳转后连接为：域名/productid,productid=id
    this.router.navigate(['audioplay',{"productid":id}])
  }
}

import { Component, OnInit } from '@angular/core';
import {Product} from "../model/Product";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../service/product.service";

@Component({
  selector: 'app-audioplay',
  templateUrl: './audioplay.component.html',
  styleUrls: ['./audioplay.component.css']
})
export class AudioplayComponent implements OnInit {
  a:Product;
  productid:string;
  constructor(private parm:ActivatedRoute,private productservice:ProductService) {
    this.a= new Product();
    this.productid=parm.snapshot.paramMap.get("productid");
    if(this.productid){
      this.getProductByNewsId(this.productid);
    }
  }

  ngOnInit() {
  }

  getProductByNewsId(id:string){
    this.productservice.getSingleProductById(id)
      .then((data:any)=>{
        this.a= new Product();
        this.a=data;
      })
  }
}

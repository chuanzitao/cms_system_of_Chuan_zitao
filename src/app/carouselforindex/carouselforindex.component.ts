import { Component, OnInit } from '@angular/core';
import {Carousel} from "../model/Carousel";
import {CarouselService} from "../service/carousel.service";
import {ConfigService} from "../service/config.service";

@Component({
  selector: 'app-carouselforindex',
  templateUrl: './carouselforindex.component.html',
  styleUrls: ['./carouselforindex.component.css']
})
export class CarouselforindexComponent implements OnInit {

  cl: Array<Carousel>; //定义变量cl

  constructor(private carouselservice: CarouselService,private config: ConfigService) {
    this.cl = new Array();
  }

  ngOnInit() {
    this.getCarouselList();
  }

  //多个查询方法
  getCarouselList() {
    this.carouselservice.getCarouselList()
      .then((data: any) => {
        this.cl = new Array();
        this.cl = data;
      })
  }

  //点击图片的方法
  onCarouselClick(url:string){
    window.open(url);   //点击图片在新窗口打开链接
  }
}


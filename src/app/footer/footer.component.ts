import { Component, OnInit } from '@angular/core';
import {Footer} from "../model/Footer";
import {FooterService} from "../service/footer.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  fl:Array<Footer>;
  constructor(private footerservice:FooterService
    ,private router:Router) {
    this.fl=new Array();

  }

  ngOnInit() {
    this.getFooterList();
  }

  getFooterList(){

    this.footerservice.getFooterList()
      .then((data:any)=>{
        this.fl=new Array();
        this.fl=data;


      })


  }
  goToFooterDetail(id:string){

    this.router.navigate(['footertail',{"footer":id}],{ preserveQueryParams: true})

  }


}

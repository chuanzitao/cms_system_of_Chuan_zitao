import { Component, OnInit } from '@angular/core';
import {AuthService} from "../service/auth.service";
import { Storage } from '@ionic/storage';
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/api";
import {ToastService} from "../service/toast.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  username:string;
  password:string;

  constructor(private authservice:AuthService
    ,private storage:Storage
    , public ref: DynamicDialogRef, public config: DynamicDialogConfig
  ,private toastservice:ToastService) { }

  ngOnInit() {
  }

  login(){

    console.log("输入的用户名是"+this.username);
    console.log("输入的密码是"+this.password);

    this.authservice.loginbyUserNameAndPass(this.username,this.password)
      .then((data:any)=>{
        //登录成功的情况
        if(data.msg=='ok'){
          this.storage.set("TOKEN",data.token)
            .then(()=>{
              //关闭登录窗
              this.ref.close();
              this.toastservice.showSuccess("登录成功！");


            })

        }
        //登录失败的情况
        else{
          //提示错误信息
          console.log("登录失败,用户名或是密码错误");
this.toastservice.showError("登录失败,用户名或是密码错误");
          //this.toastservice.showWarnning("警告信息！");
        }



      })


  }

}

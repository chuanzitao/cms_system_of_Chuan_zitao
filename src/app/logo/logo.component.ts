import { Component, OnInit } from '@angular/core';
import {Logo} from "../model/Logo";
import {LogoService} from "../service/logo.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {

  ll:Array<Logo>;
  constructor(private logoservice:LogoService
    ,private router:Router) {
    this.ll=new Array();

  }

  ngOnInit() {
    this.getLogoList();
  }

  getLogoList(){

    this.logoservice.getLogoList()
      .then((data:any)=>{
        this.ll=new Array();
        this.ll=data;


      })


  }
  goToLogoDetail(id:string){

    this.router.navigate(['logotail',{"logoid":id}],{ preserveQueryParams: true})

  }


}

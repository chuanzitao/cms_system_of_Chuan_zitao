export class SinglePage{
  singlepageid:string;
  title:string;
  author:string;
  pbdate:number;
  content:string;
  cover:string ;
  describe:string;
}

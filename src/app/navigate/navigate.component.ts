import { Component, OnInit } from '@angular/core';
import {Navigate} from "../model/Navigate";
import {Router} from "@angular/router";
import {NavigateService} from "../service/navigate.service";
import {DialogService} from "primeng/api";
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['./navigate.component.css']
})
export class NavigateComponent implements OnInit {

vl:Array<Navigate>;
  constructor(private navigateservice:NavigateService
    ,private router:Router,public dialogService: DialogService) {
    this.vl=new Array();

  }

  ngOnInit() {
    this.getNavigateList();
  }

  getNavigateList(){

    this.navigateservice.getNavigateList()
      .then((data:any)=>{
        this.vl=new Array();
        this.vl=data;


      })


  }

  goToNavigateDetail(id:string){

    this.router.navigate(['navigatedatil',{"naid":id}],{ preserveQueryParams: true})

  }


  showLogin(){

    const ref = this.dialogService.open(LoginComponent, {
      header: '用户登录'

    });

  }

}

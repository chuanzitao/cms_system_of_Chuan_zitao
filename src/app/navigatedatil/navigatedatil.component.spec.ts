import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatedatilComponent } from './navigatedatil.component';

describe('NavigatedatilComponent', () => {
  let component: NavigatedatilComponent;
  let fixture: ComponentFixture<NavigatedatilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatedatilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatedatilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

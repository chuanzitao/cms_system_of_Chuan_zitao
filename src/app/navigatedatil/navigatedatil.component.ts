import { Component, OnInit } from '@angular/core';
import {Navigate} from "../model/Navigate";
import {ActivatedRoute} from "@angular/router";
import {NavigateService} from "../service/navigate.service";

@Component({
  selector: 'app-navigatedatil',
  templateUrl: './navigatedatil.component.html',
  styleUrls: ['./navigatedatil.component.css']
})
export class NavigatedatilComponent implements OnInit {
  v:Navigate;
  naid:string;
  constructor(private parm:ActivatedRoute
    ,private navigateservice:NavigateService) {
    this.v= new Navigate();
    this.naid=parm.snapshot.paramMap.get("naid");
    if(this.naid){
      this.getNavigatByNewsId(this.naid);
    }
  }

  ngOnInit() {
  }
  getNavigatByNewsId(id:string){

    this.navigateservice.getSingleNavigateById(id)
      .then((data:any)=>{
        this.v= new Navigate();
        this.v=data;


      })

  }

}

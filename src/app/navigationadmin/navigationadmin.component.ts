import { Component, OnInit } from '@angular/core';
import {DialogService} from "primeng/api";
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-navigationadmin',
  templateUrl: './navigationadmin.component.html',
  styleUrls: ['./navigationadmin.component.css']
})
export class NavigationadminComponent implements OnInit {

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
  }
  showLogin(){

    const ref = this.dialogService.open(LoginComponent, {
      header: '用户登录'

    });

  }
}

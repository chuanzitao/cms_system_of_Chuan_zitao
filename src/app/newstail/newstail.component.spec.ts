import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewstailComponent } from './newstail.component';

describe('NewstailComponent', () => {
  let component: NewstailComponent;
  let fixture: ComponentFixture<NewstailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewstailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewstailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {News} from "../model/News";
import {ActivatedRoute} from "@angular/router";
import {NewsService} from "../service/news.service";

@Component({
  selector: 'app-newstail',
  templateUrl: './newstail.component.html',
  styleUrls: ['./newstail.component.css']
})
export class NewstailComponent implements OnInit {
  n:News;
  newsid:string;

  constructor(private parm:ActivatedRoute
    ,private newsservice:NewsService) {
    this.n= new News();
    this.newsid=parm.snapshot.paramMap.get("newsid");
    if(this.newsid){


    }


  }

  ngOnInit() {
    this.getNewsByNewsId(this.newsid);
  }


  getNewsByNewsId(id:string){

    this.newsservice.getSingleNewsById(id)
      .then((data:any)=>{
        this.n= new News();
        this.n=data;


      })

  }



}

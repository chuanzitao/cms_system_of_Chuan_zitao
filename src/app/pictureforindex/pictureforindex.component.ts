import { Component, OnInit } from '@angular/core';
import {Picture} from "../model/Picture";
import {PictureService} from "../service/picture.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-pictureforindex',
  templateUrl: './pictureforindex.component.html',
  styleUrls: ['./pictureforindex.component.css']
})
export class PictureforindexComponent implements OnInit {

  picture:Picture;
  pl:Array<Picture>;
  constructor(private pictureservice:PictureService
    ,private router:Router
  ) {
    this.picture=new Picture();
    this.pl=new Array();
  }

  ngOnInit() {
    this.getPictureList();

  }


  getPictureList(){
    this.pictureservice.getPictureList()
      .then((data:any)=>{
        this.pl=new Array();
        this.pl=data;

      })


  }

  goToDetail(pictureid:string){

    this.router.navigate(["sqdetail",{"pictureid":pictureid}]);
  }




}

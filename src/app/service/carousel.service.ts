import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Carousel} from "../model/Carousel";

@Injectable({
  providedIn: 'root'
})
export class CarouselService {

  constructor(private config:ConfigService,private http:HttpClient) { }


  private getCarouselListUrl=this.config.HOST+"/public/getCarouselList";
  getCarouselList(){
    return this.http.get(this.getCarouselListUrl).toPromise();
  }

  private getCarouselListMapUrl=this.config.HOST+"/public/getCarouselListMap";
  getCarouseListMap(){
    return this.http.get(this.getCarouselListMapUrl).toPromise();
  }

  private uplaodPicUrl=this.config.HOST+"/manage/uploadPic";
  uploadPic(file:string){
    const uploadData = new FormData();
    uploadData.append('uploadfile', file);
    return this.http.post(this.uplaodPicUrl,uploadData).toPromise();
  }

  private saveCarouselUrl=this.config.HOST+"/manage/saveCarousel";
  saveCarousel(carousel:Carousel){

    return this.http.post(this.saveCarouselUrl,carousel).toPromise();
  }

  private deleteCarouselUrl =this.config.HOST+"/manage/deleteCarousel";
  deleteCarousel(id:string){
    let parm={
      "carouselid":id
    }
    return this.http.post(this.deleteCarouselUrl,parm).toPromise();


  }

  private getSingleCarouselUrl=this.config.HOST+"/public/getSingleCarousel";

  getSingleCarousel(id:string){
    let parm={
      "carouselid":id
    }

    return this.http.post(this.getSingleCarouselUrl,parm).toPromise();
  }



}

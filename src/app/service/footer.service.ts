import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Footer} from "../model/Footer";

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  constructor(private config:ConfigService
    ,private http:HttpClient) { }
//获取新闻表列表内容
  private getFooterListUrl=this.config.HOST+'/public/getFooterList';
  getFooterList(){
    return this.http.get(this.getFooterListUrl).toPromise();
  }

//获取新闻表单个内容
  private getSingleFooterByIdUrl=this.config.HOST+"/public/getFooterById";
  getSingleFooterById(footerid:string){
    let parm={
      "footerid":footerid
    }
    return this.http.post(this.getSingleFooterByIdUrl,parm).toPromise();
  }

//保存新闻对象
  private saveFooterUrl=this.config.HOST+"/manage/saveFooter";
  saveFooter(footer:Footer){
    return this.http.post(this.saveFooterUrl,footer).toPromise();
  }

  //删除新闻对象
  private deleteFooterUrl=this.config.HOST+"/manage/deleteFooter";
  deleteFooter(id:string){
    let parm={
      "footerid":id
    }
    return this.http.post(this.deleteFooterUrl,parm).toPromise();
  }




}

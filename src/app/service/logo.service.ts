import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Logo} from "../model/Logo";

@Injectable({
  providedIn: 'root'
})
export class LogoService {

  constructor(private config: ConfigService
    , private http: HttpClient) {
  }

//获取新闻表列表内容
  private getLogoListUrl = this.config.HOST + '/public/getLogoList';

  getLogoList() {
    return this.http.get(this.getLogoListUrl).toPromise();
  }

//获取新闻表单个内容
  private getSingleLogoByIdUrl = this.config.HOST + "/public/getLogoById";

  getSingleLogoById(logoid: string) {
    let parm = {
      "logoid": logoid
    }
    return this.http.post(this.getSingleLogoByIdUrl, parm).toPromise();
  }

  private saveLogoUrl=this.config.HOST+"/manage/saveLogo";

  saveLogo(logo:Logo){

    return this.http.post(this.saveLogoUrl,logo).toPromise();
  }

  private deleteLogoUrl=this.config.HOST+"/manage/deleteLogo";

  deleteLogo(id:string){
    let parm={
      "logoid":id
    }
    return this.http.post(this.deleteLogoUrl,parm).toPromise();


  }

}



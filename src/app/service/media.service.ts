import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Media} from "../model/Media";

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private config:ConfigService,
              private http:HttpClient) { }


  private getMediaListUrl=this.config.HOST+"/public/getMediaListByType";

  getMediaList(type:string){
    let parm={
      "type":type
    }
    return this.http.post(this.getMediaListUrl,parm).toPromise();
  }

  //保存
  private saveMediaUrl=this.config.HOST+"/manage/saveMedia";
  saveMedia(media:Media){
    return this.http.post(this.saveMediaUrl,media).toPromise();
  }

  //删除
  private deleteMediaUrl=this.config.HOST+"/manage/deleteMedia";
  deleteMedia(id:string){
    let parm={
      "mediaid":id
    }
    return this.http.post(this.deleteMediaUrl,parm).toPromise();
  }



  private getMediaListByIdUrl=this.config.HOST+"/public/getMediaListById";
  getMediaListById(mediaid:string){
    let parm={
      "mediaid":mediaid
    }
    return this.http.post(this.getMediaListByIdUrl,parm).toPromise();
  }


}

import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Navigate} from "../model/Navigate";

@Injectable({
  providedIn: 'root'
})
export class NavigateService {

  constructor(private config: ConfigService
    , private http: HttpClient) { }
  private getNavigateListUrl = this.config.HOST + '/public/getNavigateList';
  getNavigateList() {

    return this.http.get(this.getNavigateListUrl).toPromise();
  }
  private getSingleNavigateByIdUrl = this.config.HOST + "/public/getNavigateById";

  getSingleNavigateById(naid: string) {
    let parm = {
      "naid": naid
    }
    return this.http.post(this.getSingleNavigateByIdUrl,parm).toPromise();
  }


  //导航区保存操作
  private saveNavigateUrl=this.config.HOST+"/manage/saveNavigate";
  saveNavigate(navigate:Navigate){
    return this.http.post(this.saveNavigateUrl,navigate).toPromise();
  }


  //删除导航区对象
  private deleteNavigateUrl=this.config.HOST+"/manage/deleteNavigate";
  deleteNavigate(id:string){
    let parm={
      "naid":id
    }
    return this.http.post(this.deleteNavigateUrl,parm).toPromise();
  }

}

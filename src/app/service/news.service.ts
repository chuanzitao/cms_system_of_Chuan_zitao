import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {News} from "../model/News";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private config:ConfigService
    ,private http:HttpClient) { }
//获取新闻表列表内容
  private getNewsListUrl=this.config.HOST+'/public/getNewsList';
  getNewsList(){
    return this.http.get(this.getNewsListUrl).toPromise();
  }

//获取新闻表单个内容
  private getSingleNewsByIdUrl=this.config.HOST+"/public/getSingleNewsById";
  getSingleNewsById(newsid:string){
    let parm={
      "newsid":newsid
    }
    return this.http.post(this.getSingleNewsByIdUrl,parm).toPromise();
  }

//保存新闻对象
  private saveNewsUrl=this.config.HOST+"/manage/saveNews";
  saveNews(news:News){
    return this.http.post(this.saveNewsUrl,news).toPromise();
  }

  //删除新闻对象
  private deleteNewsUrl=this.config.HOST+"/manage/deleteNews";
  deleteNews(id:string){
    let parm={
      "newsid":id
    }
    return this.http.post(this.deleteNewsUrl,parm).toPromise();
  }

  private getNewsListByPageUrl=this.config.HOST+"/public/getNewsListByPage";
  getNewsListBypage(page:number,pagesize:number){

    let parm={
      "page":page,
      "pagesize":pagesize
    }
    return this.http.post(this.getNewsListByPageUrl,parm).toPromise();
  }





}

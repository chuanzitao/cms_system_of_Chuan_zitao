import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Picture} from "../model/Picture";

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  constructor(private config:ConfigService
    ,private http:HttpClient) { }

  private getPictureListUrl=this.config.HOST+"/public/getPictureList";
  getPictureList(){
    return this.http.get(this.getPictureListUrl).toPromise();
  }

  private getPictureByIdUrl= this.config.HOST+"/public/getPictureById";
  getPictureById(pictureid:string){
    let parm={
      "pictureid":pictureid
    }


    return this.http.post(this.getPictureByIdUrl,parm).toPromise()
  }
  private savePictureUrl=this.config.HOST+"/manage/savePicture";

  savePicture(picture:Picture){

    return this.http.post(this.savePictureUrl,picture).toPromise();
  }

  private deletePictureUrl=this.config.HOST+"/manage/deletePicture";

  deletePicture(id:string){
    let parm={
      "pictureid":id
    }
    return this.http.post(this.deletePictureUrl,parm).toPromise();


  }



}

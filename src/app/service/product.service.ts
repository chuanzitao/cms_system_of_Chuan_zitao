import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Product} from "../model/Product";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private config: ConfigService
    , private http: HttpClient) {
  }

  private getProductListUrl = this.config.HOST + '/public/getProductList';

  getProductList() {

    return this.http.get(this.getProductListUrl).toPromise();
  }

  private getSingleProductByIdUrl = this.config.HOST + "/public/getProductById";

  getSingleProductById(productid: string) {
    let parm = {
      "productid": productid
    }
    return this.http.post(this.getSingleProductByIdUrl,parm).toPromise();
  }


  //保存
  private saveProductUrl=this.config.HOST+"/manage/saveProduct";
  saveProduct(product:Product){
    return this.http.post(this.saveProductUrl,product).toPromise();
  }

  //删除
  private deleteProductUrl=this.config.HOST+"/manage/deleteProduct";
  deleteProduct(id:string){
    let parm={
      "productid":id
    }
    return this.http.post(this.deleteProductUrl,parm).toPromise();
  }

}

import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";
import {SinglePage} from "../model/SinglePage";

@Injectable({
  providedIn: 'root'
})
export class SinglepageService {

  constructor(private config:ConfigService
    ,private http:HttpClient) { }

  private getSinglePageListUrl=this.config.HOST+"/public/getSinglePageList";
  getSinglePageList(){
    return this.http.get(this.getSinglePageListUrl).toPromise();
  }

  private getSinglePageByIdUrl= this.config.HOST+"/public/getSinglePageById";
  getSinglePageById(spid:string){
    let parm={
      "singlepageid":spid
    }


    return this.http.post(this.getSinglePageByIdUrl,parm).toPromise()
  }
  private saveSinglePageUrl=this.config.HOST+"/manage/saveSinglePage";

  saveSinglePage(singlepage:SinglePage){

    return this.http.post(this.saveSinglePageUrl,singlepage).toPromise();
  }

  private deleteSinglePageUrl=this.config.HOST+"/manage/deleteSinglePage";

  deleteSinglePage(id:string){
    let parm={
      "singlepageid":id
    }
    return this.http.post(this.deleteSinglePageUrl,parm).toPromise();


  }



}

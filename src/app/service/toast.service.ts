import { Injectable } from '@angular/core';
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private messageService: MessageService) { }
  //显示成功信息
  public showSuccess(msg:string){
    this.messageService.add({severity:'success', summary:'成功', detail:msg});
  }


  //显示警告信息
  public showWarnning(msg:string){
    this.messageService.add({severity:'warn', summary:'警告', detail:msg});
  }


//显示错误信息
  public showError(msg:string){
    this.messageService.add({severity:'error', summary:'错误', detail:msg});
  }

}

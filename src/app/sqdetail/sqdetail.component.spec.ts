import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SqdetailComponent } from './sqdetail.component';

describe('SqdetailComponent', () => {
  let component: SqdetailComponent;
  let fixture: ComponentFixture<SqdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SqdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SqdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

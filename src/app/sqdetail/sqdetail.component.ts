import { Component, OnInit } from '@angular/core';
import {SinglePage} from "../model/SinglePage";
import {ActivatedRoute} from "@angular/router";
import {SinglepageService} from "../service/singlepage.service";

@Component({
  selector: 'app-sqdetail',
  templateUrl: './sqdetail.component.html',
  styleUrls: ['./sqdetail.component.css']
})
export class SqdetailComponent implements OnInit {
  singlepage:SinglePage;
  spid:string;
  constructor(private parm:ActivatedRoute
    ,private singlepageservice:SinglepageService) {
    this.singlepage=new SinglePage();
    this.spid=parm.snapshot.paramMap.get("spid");
    this.getSinglePageById()
  }

  ngOnInit() {


  }

  getSinglePageById(){

    this.singlepageservice.getSinglePageById(this.spid)
      .then((data:any)=>{

        this.singlepage=new SinglePage();
        this.singlepage=data;


      })

  }


}

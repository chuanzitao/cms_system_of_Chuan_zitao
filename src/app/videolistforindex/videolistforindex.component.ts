import { Component, OnInit } from '@angular/core';
import {Media} from "../model/Media";
import {MediaService} from "../service/media.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-videolistforindex',
  templateUrl: './videolistforindex.component.html',
  styleUrls: ['./videolistforindex.component.css']
})
export class VideolistforindexComponent implements OnInit {
  ml:Array<Media>
  constructor(private mediaservice:MediaService,private router:Router) {
    this.ml=new Array();
  }

  ngOnInit() {
    this.getMediaList();
  }

  getMediaList(){

    this.mediaservice.getMediaList("VIDEO")

      .then((data:any)=>{
        this.ml=new Array();
        this.ml=data;

      })

  }

  goToNewsDetail(id:string){

    this.router.navigate(['videoplay',{"mediaid":id}])

  }

}

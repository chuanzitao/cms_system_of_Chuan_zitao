import { Component, OnInit } from '@angular/core';
import {Media} from "../model/Media";
import {MediaService} from "../service/media.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-videoplay',
  templateUrl: './videoplay.component.html',
  styleUrls: ['./videoplay.component.css']
})
export class VideoplayComponent implements OnInit {

  m:Media;
  mediaid:string;
  constructor(private parm:ActivatedRoute,private mediaservice:MediaService) {
    this.m=new Media();
    this.mediaid=this.parm.snapshot.paramMap.get("mediaid");
    if(this.mediaid){
      this.getMediaListId(this.mediaid);
    }
  }

  ngOnInit() {

  }

  getMediaListId(id:string){

    this.mediaservice.getMediaListById(id)
      .then((data:any)=>{
        this.m=new Media();
        this.m=data;
      })

  }

}
